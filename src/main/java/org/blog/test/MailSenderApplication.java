package org.blog.test;

import org.blog.test.email.model.TestEmail;
import org.blog.test.email.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MailSenderApplication implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(MailSenderApplication.class, args);
    }

    @Autowired
    EmailService emailService;

    @Override
    public void run(String... args) throws Exception {
        emailService.sendMail(new TestEmail("dont_reply", "test_address@naver.com", "test email", "this is the test"));
    }
}
