package org.blog.test.email.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TestEmail {
    private String sender;

    private String recipient;

    private String subject;

    private String content;
}
