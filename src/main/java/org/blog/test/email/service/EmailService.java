package org.blog.test.email.service;

import org.blog.test.email.model.TestEmail;

import javax.mail.MessagingException;

public interface EmailService {
    void sendMail(TestEmail testEmail) throws MessagingException;
}
